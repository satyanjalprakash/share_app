var config = require('config.json');
var express = require('express');
var router = express.Router();
var sharepriceService = require('services/share_data_update.service');

// routes

router.get('/updateniftyprice', function (req, res) {
    sharepriceService.update_nifty_price().then(function (data) {
        res.sendStatus(200);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });

});


router.post('/upload_data', function (req, res) {
    sharepriceService.upload_data(req).then(function (data) {
        res.sendStatus(200);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
});

router.get('/get_nifty_data', function (req, res) {
    sharepriceService.get_nifty_data().then(function (details) {
        if (details) {
            res.send(details);
        } else {
            res.sendStatus(404);
        }
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
});



module.exports = router;




