var config = require('config.json');
var express = require('express');
var router = express.Router();
var sharedetailService = require('services/share_details.service');
var sharehistoryService = require('services/share_history.service');

var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('share_details');

// routes
router.post('/create', createShare);
router.post('/update', updateShare);
router.post('/details', detailsShare);
router.get('/all_shares_list/:user_id', allShareList);
router.post('/delete', deleteShare);
router.post('/get_market_price', getMarketPrice);

module.exports = router;

function createShare(req, res) {
    sharedetailService.create(req.body).then(function () {
        sharehistoryService.create(req.body).then( function () {
            res.sendStatus(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });

    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function updateShare(req, res) {
    sharedetailService.update(req.body).then(function () {
        sharehistoryService.update(req.body).then( function () {
            res.sendStatus(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });

    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function detailsShare(req, res) {
    sharehistoryService.details(req.body).then(function (details) {
        if (details) {
            res.send(details);
        } else {
            res.sendStatus(404);
        }
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

function deleteShare(req, res) {
    sharedetailService.deleteSharebyName(req.body).then(function () {
        sharehistoryService.deleteHistory(req.body).then( function () {
            res.sendStatus(200);
        }).catch(function (err) {
            res.status(400).send(err);
        });
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

function allShareList(req, res) {
    sharedetailService.getSharesAll(req.params.user_id).then(function (details) {
        if (details) {
            res.send(details);
        } else {
            res.sendStatus(404);
        }
    })
    .catch(function (err) {
        res.status(400).send(err);
    });

}

function getMarketPrice(req, res) {
    full_url_path = 'https://www.google.com/finance/info?q=NSE:';
    full_url_path += req.body.id;

    var request = require('request');

    request(full_url_path, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            json_data = (body);
            res.send(json_data);
        }
    })
}


