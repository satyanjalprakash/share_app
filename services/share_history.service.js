var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('share_history');

var service = {};

service.create = create;
service.update = update;
service.details = details;
service.deleteHistory = deleteHistory;

module.exports = service;


function create(userParam) {
    var deferred = Q.defer();

        // var now = userParam.share_date;
        // var jsonDate = now;

        db.share_history.insert(
            {name :userParam.name,
                nse_id : userParam.nse_id,
                share_qty : parseFloat(userParam.share_qty),
                share_price : parseFloat(userParam.share_price),
                total_price : parseFloat(userParam.total_price),
                user_id : userParam.user,
                created_on : userParam.share_date,
                purchase_type : userParam.purchase_type
            },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });

    return deferred.promise;
}

function update(userParam) {
    var deferred = Q.defer();

    // var now = userParam.edit_share_date;
    // var jsonDate = now;

    db.share_history.insert(
        {name :userParam.edit_name,
            nse_id : '',
            share_qty : parseFloat(userParam.edit_share_qty),
            share_price : parseFloat(userParam.edit_share_price),
            total_price : parseFloat(userParam.edit_total_price),
            user_id : userParam.user,
            created_on : userParam.edit_share_date,
            purchase_type : userParam.purchase_type
        },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function details(userParam) {
    var deferred = Q.defer();

    db.share_history.find(
        { user_id : userParam[1], name : userParam[0] },
        function (err, share) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (share) {
                var share_det = share.toArray();
                deferred.resolve(share_det);
            } else {
                // user not found
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function deleteHistory(detailsShare) {
    var deferred = Q.defer();

    db.share_history.remove(
        { name: detailsShare.name, user_id: detailsShare.user_id},
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve();
        });

    return deferred.promise;
}
