var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var Q = require('q');

var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('nifty_prices');

var service = {};

service.update_nifty_price = update_nifty_price;
service.upload_data = upload_data;
service.get_nifty_data = get_nifty_data;

module.exports = service;


function update_nifty_price() {
    var deferred = Q.defer();
    var google_url = 'https://www.google.com/finance/historical?q=NSE:NIFTY';
    var request = require('request');
    var cheerio = require('cheerio');
    var moment = require('moment');
    var async = require('async');
    var share_datas = [];

    request(google_url, function (error, response, html) {
        var $ = cheerio.load(html);
        $('table').each(function(i, elem) {
            if (i > 2) {
                var tre = $(this).text();
                var arr = tre.split("\n\n");
                for(var it = 0; it < arr.length; it++) {
                    data = arr[it];
                    share_datas.push(data);
                }
            }
        });
        console.log(share_datas);

        var date = null;
        var open = null;
        var high = null;
        var low = null;
        var close = null;
        var share_traded = null;
        var turn_over = null;
        var string_date = null;
        var count = 0;
        async.eachSeries(share_datas,function(item,callback) {
            if (count != 0 && count != 1) {
                count += 1;
                var line = item.split('\n');
                date = line[0];
                var dt = new Date(date);
                var moment_string_date = moment(date).format('DD/MM/YYYY');
                string_date = moment_string_date.toString();
                open = line[1].replace(',', '');
                high = line[2].replace(',', '');
                low = line[3].replace(',', '');
                close = line[4].replace(',', '');
                share_traded = parseFloat(0.0);
                turn_over = parseFloat(0.0);

                db.nifty_prices.findOne(
                    {date: string_date},
                    function (err, user) {
                        if (err) deferred.reject(err.name + ': ' + err.message);
                        if (user) {
                            callback(null, user);
                        } else {
                            console.log(string_date, "created_share");
                            createShare();
                            callback(null, user);
                        }
                    });
            } else {
                count += 1;
                callback(null, null);
            }
        },function(err) {
            if (err) throw err;
            console.log("done");

        });

        function createShare() {
            db.nifty_prices.insert(
            {
                date: string_date,
                open: parseFloat(open),
                high: parseFloat(high),
                low: parseFloat(low),
                close: parseFloat(close),
                share_traded: share_traded,
                turnover: turn_over
            },
            function (err, doc) {
                if (err) {
                }
            });
        }
    });

    deferred.resolve();

    return deferred.promise;
}

function get_nifty_data() {
    var deferred = Q.defer();

    db.nifty_prices.find(
        function (err, share) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (share) {
                var share_det = share.toArray();
                deferred.resolve(share_det);
            } else {
                // user not found
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function upload_data(datas) {
    // var deferred = Q.defer();
    var shares_datas = datas.body;
    for(var i = 0; i < shares_datas.length; i++) {
        var share = shares_datas[i];
        var date = new Date(share[0]);
        date.setMinutes(date.getMinutes()+330);
        db.nifty_prices.insert(
            {
                date: share[0],
                open: parseFloat(share[1]),
                high: parseFloat(share[2]),
                low: parseFloat(share[3]),
                close: parseFloat(share[4]),
                share_traded: parseFloat(share[5]),
                turnover: parseFloat(share[6])
            },
            function (err, doc) {
                if (err) {
                    console.log(date);
                }
            });
    }
    return "";
}



