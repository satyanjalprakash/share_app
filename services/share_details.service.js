var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('share_details');

var service = {};

service.create = create;
service.update = update;
service.getSharesAll = getAll;
service.deleteSharebyName = deleteShare;

module.exports = service;


function create(userParam) {
    var deferred = Q.defer();

    // validation
    db.share_details.findOne(
        { name: userParam.name, user_id: userParam.user },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (user) {
                deferred.reject(userParam.name + ' already exist');
            } else {
                createShare();
            }
        });

    function createShare() {
        // set user object to userParam without the cleartext password

        db.share_details.insert(
            {name :userParam.name,
             nse_id : userParam.nse_id,
             share_qty : parseFloat(userParam.share_qty),
             share_price : parseFloat(userParam.share_price),
             total_price : parseFloat(userParam.total_price),
             user_id : userParam.user
            },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function update(userParam) {
    var deferred = Q.defer();
    var total_price = 0;
    var total_qty = 0;
    var share_price = 0;

    // validation
    db.share_details.findOne(
        { name:userParam.edit_name, user_id: userParam.user },
        function (err, old_share) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (old_share) {
                total_qty = parseFloat(old_share.share_qty);
                share_price = parseFloat(old_share.share_price);
                total_price = parseFloat(old_share.total_price);
                updateShare();
                // deferred.reject(userParam.name + '" already exist');
            } else {
                deferred.reject(userParam.edit_name + '" went wrong');
            }
        });

    function updateShare() {
        // fields to update
        var edit_total_price = parseFloat(userParam.edit_total_price);
        var edit_share_qty = parseFloat(userParam.edit_share_qty);
        var final_share_qty = 0;
        var final_total_price = 0;
        var full_total_price = 0;
        if (userParam.purchase_type == "buy") {
            full_total_price = (total_price + edit_total_price)/(total_qty + edit_share_qty);
            final_share_qty = total_qty + edit_share_qty;
            final_total_price = total_price +  edit_total_price;
        } else {
            full_total_price = (total_price + (edit_total_price*(-1)))/(total_qty + (edit_share_qty*(-1)));
            final_share_qty = total_qty + (edit_share_qty*(-1));
            final_total_price = total_price +  (edit_total_price*(-1));
        }


        var set = {
            // name: userParam.edit_name,
            share_qty: final_share_qty,
            share_price: full_total_price,
            total_price: final_total_price
        };

        // update password if it was entered
        db.share_details.update(
            { name:userParam.edit_name, user_id: userParam.user },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function getAll(userParam) {
    var deferred = Q.defer();
    db.share_details.find({ user_id: userParam },
        function (err, all_share) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (all_share) {
                var share_det = all_share.toArray();
                deferred.resolve(share_det);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function deleteShare(details) {
    var deferred = Q.defer();

    db.share_details.remove(
        { name: details.name, user_id: details.user_id },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve();
        });

    return deferred.promise;
}