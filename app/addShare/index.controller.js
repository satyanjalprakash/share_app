(function () {
    'use strict';

    angular
        .module('app')
        .controller('AddShare.IndexController', Controller);

    function Controller($state, $scope, UserService, ShareService, FlashService, $filter) {
        var vm = this;

        vm.details = {};

        $scope.createShares = function() {
            UserService.GetCurrent().then(function (user) {
                vm.details.user = user._id;
                vm.details.share_date = $filter('date')(vm.details.share_date, "dd/MM/yyyy");
                vm.details.purchase_type = "buy";
                ShareService.Create(vm.details).then(function () {
                    FlashService.Success('Share added');
                    vm.details = null;

                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
            });
        };

        $scope.cal_totalprice = function() {
            var quantity = vm.details.share_qty;
            var price = vm.details.share_price;
            vm.details.total_price = quantity*price;
        };

        $scope.format = 'dd-MMM-yyyy';
        $scope.open = function() {
            $scope.popup.opened = true;
        };
        $scope.popup = {
            opened: false
        };

    }

})();