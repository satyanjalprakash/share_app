(function () {
    'use strict';
    angular
        .module('app')
        .controller('UploadCSV.IndexController', Controller);

    function Controller($scope, $http, $timeout) {
        var vm = this;
        var lines = [];
        var nifty_lines = [];

        $scope.processData = function() {
            var allText = null;
            var Url   = "app-content/datas/my_Data_Nifty.csv";
            $http.get(Url).then(function(response) {
                allText = response.data;
                var allTextLines = allText.split(/\r\n|\n/);
                var headers = allTextLines[0].split(',');
                for (var i = 0; i < (allTextLines.length); i++) {
                    // split content based on comma
                    var data = allTextLines[i].split(',');
                    if (data.length == headers.length) {
                        var tarr = [];
                        for (var j = 0; j < headers.length; j++) {
                            tarr.push(data[j]);
                        }
                        lines.push(tarr);
                    }
                }
            });
        };
        $scope.processData();

//--------------------------------------------
    }
})();