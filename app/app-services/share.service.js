(function () {
    'use strict';

    angular
        .module('app')
        .factory('ShareService', Service);

    function Service($http, $q) {
        var service = {};

        service.AllShare = AllShare;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.Details = Details;
        service.getMarketPrice = getMarketPrice;

        return service;


        function Create(share_details) {
            return $http.post('/api/share_details/create', share_details).then(handleSuccess, handleError);
        }

        function Update(share_details) {
            return $http.post('/api/share_details/update', share_details).then(handleSuccess, handleError);
        }

        function Delete(share_details) {
            return $http.post('/api/share_details/delete' , share_details).then(handleSuccess, handleError);
        }

        function AllShare(user_id) {
            return $http.get('/api/share_details/all_shares_list/'+user_id).then(handleSuccess, handleError);
        }

        function getMarketPrice(url_details) {
            return $http.post('/api/share_details/get_market_price', url_details).then(handleSuccess, handleError);
        }

        function Details(share_details) {
            return $http.post('/api/share_details/details', share_details).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
