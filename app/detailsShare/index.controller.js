(function () {
    'use strict';
    angular
        .module('app')
        .controller('DetailsShare.IndexController', Controller)
        .filter('makePositive', function() {
            return function(num) { return Math.abs(num); }
        });

    function Controller($stateParams, $scope, $state) {
        var vm = this;
        var x_axis = ['x'];
        var y_axis = ['Buy'];
        var y_axis2 = ['Sell'];
        $scope.showgraphs = false;
        if ($stateParams.myParam != null) {
            $scope.share_details = $stateParams.myParam.response;
            angular.forEach($scope.share_details, function (share_data) {
                var st = share_data.created_on;
                var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
                var dt = new Date(st.replace(pattern,'$3-$2-$1'));
                share_data.created_on = dt;
                x_axis.push(dt);
                if (share_data.purchase_type == "buy") {
                    y_axis.push(share_data.share_price);
                    y_axis2.push(null);
                } else {
                    y_axis.push(null);
                    y_axis2.push(share_data.share_price)
                }
            });
        } else {
            $state.go('home');
        }

        $scope.show_graphs = function () {
            plot_graph();
            $scope.showgraphs = true;
        };

        $scope.hide_graphs = function () {
            $scope.showgraphs = false;
        };

        var plot_graph = function () {
            var chart = c3.generate({
                bindto: '#graphs',
                data: {
                    x: 'x',
                    columns: [
                        x_axis,
                        y_axis,
                        y_axis2
                    ],
                    colors: {
                        Buy: 'green',
                        Sell: 'red'
                    },
                    axes: {
                        Sell: 'y2'
                    }
                },
                axis: {
                    x: {
                        type: 'timeseries',
                        tick: {
                            format: '%d-%b-%Y'
                        },
                        label: {
                            text: 'Dates',
                            position: 'outer-right'
                        }
                    },
                    y: {
                        label: {
                            text: 'Prices',
                            position: 'outer-middle'
                        },
                        tick: {
                            format: d3.format("s")
                        }
                    },
                    y2: {
                        show: false
                    }
                },
                zoom: {
                    enabled: true
                },
                tooltip: {
                    format: {
                        value: d3.format(',') // apply this format to both y and y2
                    }
                }
            });
            chart.resize({height: 450});
        };

    }

})();