﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('Home.IndexController', Controller);

    function Controller($scope, UserService, ShareService, FlashService, $state, $filter) {
        var vm = this;

        $scope.edit_box = false;
        vm.user = null;
        vm.details = null;
        vm.edit_details = {};
        var current_user = null;

        initController();

        function initController() {
            // get current user
            UserService.GetCurrent().then(function (user) {
                vm.user = user;
                current_user = user._id;
                shareList(current_user);
            });
        }

        function shareList(current_user) {
            ShareService.AllShare(current_user).then(function (shares) {
                angular.forEach(shares, function (share_data) {
                    var nse_id = {id: share_data.nse_id};

                    ShareService.getMarketPrice(nse_id).then(function (response) {
                        var data = JSON.parse(response.slice(3, -1));
                        var share_market_price = data[0].l;
                        var sample = share_market_price.replace(',', '');
                        share_data.market_price = parseFloat(sample);
                        var perc_gain = (((((share_data.share_qty)*(share_data.market_price)) / share_data.total_price)- 1)*100);
                        share_data.percentage_gain = perc_gain;
                    })
                    .catch(function (error) {
                        FlashService.Error(error);
                    });
                });
                $scope.all_shares = shares;
            });
        }

        $scope.cal_edit_totalprice = function () {
            var quantity = vm.edit_details.edit_share_qty;
            var price = vm.edit_details.edit_share_price;

            vm.edit_details.edit_total_price = quantity * price;
        };

        $scope.addShare = function (share_name) {
            $scope.edit_box = true;
            vm.edit_details.edit_name = share_name;

        };

        $scope.canceladdShare = function (share_name) {
            $scope.edit_box = false;
        };

        $scope.deleteShare = function (share) {
            ShareService.Delete(share).then(function () {
                shareList(current_user);
            })
        };

        $scope.updateShare = function () {
            vm.edit_details.user = current_user;
            vm.edit_details.edit_share_date = $filter('date')(vm.edit_details.edit_share_date, "dd/MM/yyyy");
            ShareService.Update(vm.edit_details).then(function () {
                vm.edit_details = {};
                $scope.edit_box = false;
                shareList(current_user);
            })
        };

        $scope.detailsShare = function (share_name) {
            var details = [share_name, current_user]
            ShareService.Details(details).then(function (share_details) {
                $state.go('detailsShare', {myParam: {response: share_details}});
            })
        };

        $scope.format = 'dd-MMM-yyyy';
        $scope.open = function() {
            $scope.popup.opened = true;
        };
        $scope.popup = {
            opened: false
        };


    }

})();