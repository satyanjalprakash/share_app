(function () {
    'use strict';
    angular
        .module('app')
        .controller('ChartsGraphs.IndexController', Controller);

    function Controller($scope, $http, $timeout, $filter) {
        var vm = this;
        var lines = [];
        var nifty_lines = [];

        $scope.processData = function() {
            var allText = null;
            var Url   = "app-content/datas/my_Data_Nifty.csv";
            $http.get(Url).then(function(response) {
                allText = response.data;
                var allTextLines = allText.split(/\r\n|\n/);
                var headers = allTextLines[0].split(',');
                for (var i = 0; i < (allTextLines.length); i++) {
                    // split content based on comma
                    var data = allTextLines[i].split(',');
                    if (data.length == headers.length) {
                        var tarr = [];
                        for (var j = 0; j < headers.length; j++) {
                            tarr.push(data[j]);
                        }
                        lines.push(tarr);
                    }
                }
            });
        };



        $scope.processData2 = function() {
            var allText = null;
            var Url   = "app-content/datas/nifty_data.csv";
            $http.get(Url).then(function(response) {
                allText = response.data;
                var allTextLines = allText.split(/\r\n|\n/);
                var headers = allTextLines[0].split(',');
                for (var i = 0; i < (allTextLines.length); i++) {
                    // split content based on comma
                    var data = allTextLines[i].split(',');
                    if (data.length == headers.length) {
                        var tarr = [];
                        for (var j = 0; j < headers.length; j++) {
                            tarr.push(data[j]);
                        }
                        nifty_lines.push(tarr);
                    }
                }
            });
        };

        $scope.processData();
        $scope.processData2();

//--------------------------------------------
        var share_datas = [];
        var x_axis = ['x'];
        var y_axis = ['Nifty_Price'];
        var y_axis2 = ['My_Price'];

        $timeout( function() {
        $http.get("/views/share_update/get_nifty_data").then(function (response) {
            var db_nifty_date = response.data;
            for(var tr=0; tr < db_nifty_date.length ; tr++) {
                for(var i=1; i < lines.length ; i++) {
                    var my_data = lines[i];
                    var my_date = my_data[0];
                    var my_dt = new Date(my_date);

                    var share_data = db_nifty_date[tr];
                    var date = share_data.date;
                    var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
                    var dt = new Date(date.replace(pattern,'$3-$2-$1'));
                    if (dt.getDate() == my_dt.getDate() && dt.getMonth() == my_dt.getMonth() && dt.getYear() == my_dt.getYear()) {
                        var closed_value = share_data.close;
                        var date_string = $filter('date')(dt, "dd/MM/yyyy");
                        // var x_y_axis = [date_string, share_data[1], share_data[2], share_data[3], (closed_value), share_data[5], share_data[6]];
                        // share_datas.push(x_y_axis);
                        x_axis.push(dt);
                        y_axis.push(parseInt(closed_value));
                        y_axis2.push(parseInt(my_data[3]));
                    }
                }
            }

            $timeout( function() {
                var chart = c3.generate({
                    bindto: '#chart',
                    data: {
                        x: 'x',
                        columns: [
                            x_axis,
                            y_axis,
                            y_axis2
                        ],
                        colors: {
                            My_Price : 'green'
                        },
                        axes: {
                            My_Price: 'y2'
                        }
                        // types: {
                        //     Nifty_Price: 'area',
                        //     My_Price: 'area-spline'
                        // }
                    },
                    axis: {
                        x: {
                            type: 'timeseries',
                            tick: {
                                format: '%d-%b-%Y'
                            },
                            label: {
                                text: 'Dates',
                                position: 'outer-right'
                            }
                        },
                        y: {
                            label: {
                                text: 'Prices',
                                position: 'outer-middle'
                            },
                            tick: {
                                format: d3.format("s")
                            }
                        },
                        y2: {
                            padding: {top: 0, bottom: 191},
                            show: true
                        }
                    },
                    grid: {
                        y: {
                            lines: [
                                {value: 509509, text: 'Base Line', axis: 'y2', position: 'start'}
                            ]
                        }
                    },
                    zoom: {
                        enabled: true
                    },
                    tooltip: {
                        format: {
                            // title: function (d) { return 'Data ' + d; },
                            value: d3.format(',') // apply this format to both y and y2
                        }
                    }
                });
                chart.resize({height:450})

            },800);
        });
        },1800);
    }
})();